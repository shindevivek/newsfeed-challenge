//
//  BookMark+CoreDataProperties.h
//  DailyFeed
//
//  Created by Vivek LogTera on 01/11/15.
//  Copyright © 2015 Logtera. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "BookMark.h"

NS_ASSUME_NONNULL_BEGIN

@interface BookMark (CoreDataProperties)

@property (nullable, nonatomic, retain) NSNumber *bookMarkId;
@property (nullable, nonatomic, retain) id url;

@end

NS_ASSUME_NONNULL_END
