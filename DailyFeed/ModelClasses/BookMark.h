//
//  BookMark.h
//  DailyFeed
//
//  Created by Vivek LogTera on 01/11/15.
//  Copyright © 2015 Logtera. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface BookMark : NSManagedObject

// Insert code here to declare functionality of your managed object subclass

@end

NS_ASSUME_NONNULL_END

#import "BookMark+CoreDataProperties.h"
