//
//  BookMark+CoreDataProperties.m
//  DailyFeed
//
//  Created by Vivek LogTera on 01/11/15.
//  Copyright © 2015 Logtera. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "BookMark+CoreDataProperties.h"

@implementation BookMark (CoreDataProperties)

@dynamic bookMarkId;
@dynamic url;

@end
