//
//  FeedAPI.m
//  DailyFeed
//
//  Created by Vivek LogTera on 31/10/15.
//  Copyright © 2015 Logtera. All rights reserved.
//

#import "Utility.h"
#import "Reachability.h"
#import "AppDelegate.h"
@implementation Utility


+(NSArray *)fetchDatafromTable :(NSString *)tableName predicate:(NSPredicate *)predicate{
    NSArray *array = nil;
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context =  [appDelegate managedObjectContext];
    
    if (context){
        
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        NSEntityDescription *fetchentity = [NSEntityDescription entityForName:tableName
                                                       inManagedObjectContext:context];
        [fetchRequest setEntity:fetchentity];
        
        if (predicate !=Nil) {
            [fetchRequest setPredicate:predicate];
        }
        
        NSError *error = nil;
        array = [context executeFetchRequest:fetchRequest error:&error];
        return array;
    } else {
        return array;
        
    }
}


@end
