//
//  MySharedData.m
//  ButtonAndImage
//
//  Created by Rahul Jauhari on 3/29/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "MySharedData.h"

static MySharedData *sharedData = nil;

@implementation MySharedData

#pragma mark Singleton Methods
+ (id)sharedManager {
    @synchronized(self) {
        if (sharedData == nil)
            sharedData = [[self alloc] init];
    }
    return sharedData;
}

- (id)init {
    if (self = [super init]) {
        
        self.articalDic =[[NSMutableDictionary alloc]init];
        
    }
   
    return self;
}
- (void)dealloc {
    // Should never be called, but just here for clarity really.
}

@end
