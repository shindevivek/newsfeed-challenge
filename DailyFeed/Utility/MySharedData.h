//
//  MySharedData.h
//  ButtonAndImage
//
//  Created by Rahul Jauhari on 3/29/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>

@interface MySharedData : NSObject{
    
}
@property (nonatomic, readwrite)NSMutableDictionary *articalDic;



+ (id)sharedManager;


@end
