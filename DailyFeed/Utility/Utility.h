//
//  FeedAPI.h
//  DailyFeed
//
//  Created by Vivek LogTera on 31/10/15.
//  Copyright © 2015 Logtera. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Utility : NSObject

+(NSArray *)fetchDatafromTable :(NSString *)tableName predicate:(NSPredicate *)predicate;

@end
