//
//  ReadModeViewController.h
//  DailyFeed
//
//  Created by Vivek LogTera on 01/11/15.
//  Copyright © 2015 Logtera. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReadModeViewController : UIViewController
@property (strong, nonatomic) IBOutlet UILabel *Title;
@property (strong, nonatomic) IBOutlet UILabel *linkSourceArticle;
@property (strong, nonatomic) IBOutlet UIImageView *imageUrl;
@property (strong, nonatomic) IBOutlet UITextView *articaleContent;


@property (strong, nonatomic) NSString *Title1;
@property (strong, nonatomic) NSString *linkSourceArticle1;
@property (strong, nonatomic) NSString *imageUrl1;
@property (strong, nonatomic) NSString *articaleContent1;
@property (strong, nonatomic) NSMutableDictionary *articalDic;




@end
