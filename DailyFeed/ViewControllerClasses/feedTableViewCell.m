//
//  feedTableViewCell.m
//  DailyFeed
//
//  Created by Vivek LogTera on 31/10/15.
//  Copyright © 2015 Logtera. All rights reserved.
//

#import "feedTableViewCell.h"

@implementation feedTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (IBAction)detailButtonPressed:(UIButton *)sender {
    
    [self.delegate showDetailsofTopic:sender];
    
}
- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}



@end
