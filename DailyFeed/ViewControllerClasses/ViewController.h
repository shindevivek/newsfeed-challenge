//
//  ViewController.h
//  DailyFeed
//
//  Created by Vivek LogTera on 31/10/15.
//  Copyright © 2015 Logtera. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,UISearchBarDelegate,UISearchDisplayDelegate>

@property (strong, nonatomic) IBOutlet UITableView *feedTableView;
@property (strong,nonatomic) NSMutableArray *filteredCandyArray;
@property IBOutlet UISearchBar *candySearchBar;
@property (nonatomic) BOOL isBookMark;


+(void)addActivityViewcontroller:(UIView *)view;
+(void)removeActivity:(UIView*)view;
- (BOOL)connected;
- (void)connectionError:(id)sender;
@end


