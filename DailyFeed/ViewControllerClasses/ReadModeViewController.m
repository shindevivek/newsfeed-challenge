

//
//  ReadModeViewController.m
//  DailyFeed
//
//  Created by Vivek LogTera on 01/11/15.
//  Copyright © 2015 Logtera. All rights reserved.
//
#import "ViewController.h"
#import "ReadModeViewController.h"
#import "BookMark.h"
#import "AppDelegate.h"
#import "MySharedData.h"

@implementation ReadModeViewController


#define feedQueue dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT,0)



-(void)viewDidLoad{

    _Title.text = self.Title1;
    _linkSourceArticle.text = self.linkSourceArticle1;
    _articaleContent.text=_articaleContent1;
    
    [self adjustlabelFont:_Title minimumFont:6.0];
    [self adjustlabelFont:_linkSourceArticle minimumFont:6.0];

    
    ViewController *viewControllerObj =[[ViewController alloc]init];
    [ViewController addActivityViewcontroller:self.view];
    if ([viewControllerObj connected]) {
        
        dispatch_async(feedQueue, ^{
            NSURL *url = [NSURL URLWithString:_imageUrl1];
            NSData *data = [NSData dataWithContentsOfURL:url];
            [self performSelectorOnMainThread:@selector(fetchedDataForImage:)
                                   withObject:data waitUntilDone:YES];
        });
    }else{
        [viewControllerObj connectionError:self];
    }
    
    
}


-(void)adjustlabelFont :(UILabel *)titleLabel minimumFont:(float)minimumFont{
    titleLabel.minimumScaleFactor =minimumFont/titleLabel.font.pointSize;
   titleLabel.adjustsFontSizeToFitWidth = YES;
}

- (void)fetchedDataForImage:(NSData *)responseData {
    if (responseData) {
            [ViewController removeActivity:self.view];
            UIImage *img = [[UIImage alloc] initWithData:responseData];
            _imageUrl.image = img;
        
    }
    //    NSLog(@"articles: %@", articlesArray); //3
}


- (NSURL *) fileToURL:(NSString*)filename
{
    NSArray *fileComponents = [filename componentsSeparatedByString:@"."];
    NSString *filePath = [[NSBundle mainBundle] pathForResource:[fileComponents objectAtIndex:0] ofType:[fileComponents objectAtIndex:1]];
    return [NSURL fileURLWithPath:filePath];
}



- (IBAction)sharedPressed:(UIButton *)sender {
//    NSURL *url = [self fileToURL:_linkSourceArticle1];
//    NSArray *objectsToShare = @[url];
//    
//    UIActivityViewController *controller = [[UIActivityViewController alloc] initWithActivityItems:objectsToShare applicationActivities:nil];
//    
//    // Exclude all activities except AirDrop.
//    NSArray *excludedActivities = @[UIActivityTypePostToTwitter, UIActivityTypePostToFacebook,
//                                    UIActivityTypePostToWeibo,
//                                    UIActivityTypeMessage, UIActivityTypeMail,
//                                    UIActivityTypePrint, UIActivityTypeCopyToPasteboard,
//                                    UIActivityTypeAssignToContact, UIActivityTypeSaveToCameraRoll,
//                                    UIActivityTypeAddToReadingList, UIActivityTypePostToFlickr,
//                                    UIActivityTypePostToVimeo, UIActivityTypePostToTencentWeibo];
//    controller.excludedActivityTypes = excludedActivities;
//    
//    // Present the controller
//    [self presentViewController:controller animated:YES completion:nil];
//    
}


-(IBAction)addBookMark:(UIButton *)sender{
    AppDelegate *appdelegateObj =(AppDelegate *)[[UIApplication sharedApplication]delegate];
    NSManagedObjectContext *context =[appdelegateObj managedObjectContext];
    BookMark *bookMarkObj =[NSEntityDescription insertNewObjectForEntityForName:@"BookMark" inManagedObjectContext:context];
    MySharedData *sharedData =[MySharedData sharedManager];    
    bookMarkObj.url =  sharedData.articalDic;
    
    NSError *error = nil;
    if (![context save:&error]) {
        NSLog(@"can not save record");
    }else{
        UIAlertView *connectionError = [[UIAlertView alloc]initWithTitle:@"You Added New Bookmark" message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [connectionError show];

    }
    
}


- (IBAction)backPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
    self.view.userInteractionEnabled = YES;
}





@end
