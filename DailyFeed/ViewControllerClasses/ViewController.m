//
//  ViewController.m
//  DailyFeed
//
//  Created by Vivek LogTera on 31/10/15.
//  Copyright © 2015 Logtera. All rights reserved.
//





#import "ViewController.h"
#import "Reachability.h"
#import "feedTableViewCell.h"
#import "ReadModeViewController.h"
#import "MySharedData.h"
#import "Utility.h"
#import "BookMark.h"

#define FEED_URL [NSURL URLWithString:@"https://dailyhunt.0x10.info/api/dailyhunt?type=json&query=list_news"]
#define API_HIT_URL [NSURL URLWithString:@"https://dailyhunt.0x10.info/api/dailyhunt?type=json&query=api_hits"]


#define feedQueue dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT,0)



@interface ViewController ()
@property (strong, nonatomic) NSMutableArray *articlesArray;
@property (strong, nonatomic) IBOutlet UILabel *apiHits;

@end

@implementation ViewController{
    NSMutableArray *tableDataSourceArray;

}
@synthesize filteredCandyArray;
@synthesize candySearchBar;



- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.

    self.articlesArray = [[NSMutableArray alloc]init];
    self.feedTableView.hidden =YES;
    self.feedTableView.backgroundColor = [UIColor clearColor];
    self.feedTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    tableDataSourceArray = [[NSMutableArray alloc] init];
    
    
    if ([self connected]) {
        if (self.isBookMark) {
            self.articlesArray = (NSMutableArray *)[Utility fetchDatafromTable:@"BookMark" predicate:nil];
            self.feedTableView.frame = CGRectMake(0, 80, self.feedTableView.frame.size.width, self.feedTableView.frame.size.height);
            [self.feedTableView bringSubviewToFront:self.view];
            [self.view viewWithTag:10].hidden = NO;
            for (int i=11; i<=16; i++) {
                [self.view viewWithTag:i].hidden = YES;

            }
            self.feedTableView.delegate = self;
            self.feedTableView.dataSource = self;
            self.feedTableView.hidden = NO ;
            [self.feedTableView reloadData];
            [ViewController removeActivity:self.view];
            self.filteredCandyArray = [NSMutableArray arrayWithCapacity:[self.articlesArray count]];
        }else{
            [ViewController addActivityViewcontroller:self.view];
            [self.view viewWithTag:10].hidden =YES;
            [self.view viewWithTag:17].hidden =YES;
            dispatch_async(feedQueue, ^{
                NSData* data = [NSData dataWithContentsOfURL:
                                FEED_URL];
                [self performSelectorOnMainThread:@selector(fetchedData:)
                                       withObject:data waitUntilDone:YES];
            });
            
            dispatch_async(feedQueue, ^{
                NSData *hitUrl =[NSData dataWithContentsOfURL:API_HIT_URL];
                [self performSelectorOnMainThread:@selector(fetchedDataForHitAPI:)
                                       withObject:hitUrl waitUntilDone:YES];
            });
        }
        
    }else{
        [self connectionError:self];
    }
}
- (void)fetchedData:(NSData *)responseData {
    //parse out the json data
    
    if (responseData) {
        NSError* error;
        NSDictionary* json = [NSJSONSerialization
                              JSONObjectWithData:responseData //1
                              options:kNilOptions
                              error:&error];
        if (!error&&json){
            self.articlesArray = [json objectForKey:@"articles"]; //2
            self.feedTableView.delegate = self;
            self.feedTableView.dataSource = self;
            self.feedTableView.hidden = NO ;
            [self.feedTableView reloadData];
            [ViewController removeActivity:self.view];
            self.filteredCandyArray = [NSMutableArray arrayWithCapacity:[self.articlesArray count]];
        }
    }
     //    NSLog(@"articles: %@", articlesArray); //3
}

- (void)fetchedDataForHitAPI:(NSData *)responseData {
    if (responseData) {
        NSError* error;
        NSDictionary* json = [NSJSONSerialization
                              JSONObjectWithData:responseData //1
                              options:kNilOptions
                              error:&error];
        if (!error && json){
            self.apiHits.text= [json objectForKey:@"api_hits"];
            [ViewController removeActivity:self.view];
        }
    }
}





#pragma mark Content Filtering
-(void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope {
    // Update the filtered array based on the search text and scope.
    // Remove all objects from the filtered search array
    [self.filteredCandyArray removeAllObjects];
    // Filter the array using NSPredicate
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.name contains[c] %@",searchText];
    self.filteredCandyArray = [NSMutableArray arrayWithArray:[self.articlesArray filteredArrayUsingPredicate:predicate]];
}
#pragma mark - UISearchDisplayController Delegate Methods
-(BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString {
    // Tells the table data source to reload when text changes
    [self filterContentForSearchText:searchString scope:
     [[self.searchDisplayController.searchBar scopeButtonTitles] objectAtIndex:[self.searchDisplayController.searchBar selectedScopeButtonIndex]]];
    // Return YES to cause the search result table view to be reloaded.
    return YES;
}

-(BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchScope:(NSInteger)searchOption {
    // Tells the table data source to reload when scope bar selection changes
    [self filterContentForSearchText:self.searchDisplayController.searchBar.text scope:
     [[self.searchDisplayController.searchBar scopeButtonTitles] objectAtIndex:searchOption]];
    // Return YES to cause the search result table view to be reloaded.
    return YES;
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    //#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    //warning Incomplete method implementation.
    // Return the number of rows in the section.
    //return array1.count;
//    return self.articlesArray.count;
    
    // Check to see whether the normal table or search results table is being displayed and return the count from the appropriate array
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        return [filteredCandyArray count];
    } else {
        return [self.articlesArray count];
    }
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *CellIdentifier = @"feedCell";
    feedTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath ];
    
    if (cell == nil) {
        cell = [[feedTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    cell.delegate = self;
   
    if (self.articlesArray!=nil && self.articlesArray.count>0 && indexPath.row < self.articlesArray.count){
        NSMutableDictionary *articalDetailsDic;
        if (self.isBookMark) {
          BookMark *bookMarkObj =  [self.articlesArray objectAtIndex:indexPath.row];
             articalDetailsDic=bookMarkObj.url;
        }else{
            articalDetailsDic  = [self.articlesArray objectAtIndex:indexPath.row];
        }
        // Check to see whether the normal table or search results table is being displayed and set the Candy object from the appropriate array
    cell.title.text  =  (NSString *)[articalDetailsDic objectForKey:@"title"];
    cell.title.minimumScaleFactor = 6.0/cell.title.font.pointSize;
    cell.title.adjustsFontSizeToFitWidth = YES;
        cell.detailButton.tag = indexPath.row;
    int randomNum = arc4random() % 7;
        cell.loadImageFromUrl.image =[UIImage imageNamed:[NSString stringWithFormat:@"icon%i",randomNum]];
    
//        NSURL *url = [NSURL URLWithString:(NSString *)[articalDetailsDic objectForKey:@"image"]];
//        NSData *data = [NSData dataWithContentsOfURL:url];
//        UIImage *img = [[UIImage alloc] initWithData:data];
//        cell.loadImageFromUrl.image = img;
    }
    return cell;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 40;

}

- (void)showDetailsofTopic:(UIButton *)sender{
    NSLog(@"go on next skill");
    
    NSMutableDictionary *articalDetailsDic ;
    if (self.isBookMark) {
        BookMark *bookMarkObj =  [self.articlesArray objectAtIndex:sender.tag];
        articalDetailsDic=bookMarkObj.url;
    }else{
        articalDetailsDic  = [self.articlesArray objectAtIndex:sender.tag];
    }
    
    UIStoryboard *mainStoryboard;
    ReadModeViewController *vc;
    mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"Read Mode"];
    vc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    vc.Title1 = (NSString *)[articalDetailsDic objectForKey:@"title"];
    vc.linkSourceArticle1= (NSString *)[articalDetailsDic objectForKey:@"source"];
    vc.imageUrl1 = (NSString *)[articalDetailsDic objectForKey:@"image"];
    vc.articaleContent1 =(NSString *)[articalDetailsDic objectForKey:@"content"];
    MySharedData *sharedData =[MySharedData sharedManager];
    sharedData.articalDic = articalDetailsDic; // [self presentViewController:vc animated:YES completion:nil];
    
//    NSLog(@"%@",sharedData.articalDic);
    [[self navigationController]pushViewController:vc animated:YES];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

+(void)addActivityViewcontroller:(UIView *)view{
    UIActivityIndicatorView  *indicator = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    indicator.tag=50;
    indicator.frame = CGRectMake(view.frame.size.width/2-25,view.frame.size.height/2-25,50,50);
    [view addSubview:indicator];
     view.userInteractionEnabled =NO;
    [indicator startAnimating];
}

+(void)removeActivity:(UIView*)view{
    [[view viewWithTag:50] removeFromSuperview];
    view.userInteractionEnabled =YES;
}

- (BOOL)connected
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    return !(networkStatus == NotReachable);
}

- (void)connectionError:(id)sender
{
    
    UIAlertView *connectionError = [[UIAlertView alloc]initWithTitle:@"Connection Not Available" message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [connectionError show];
}

- (IBAction)bookMarkPressed:(id)sender {
    
    UIStoryboard *mainStoryboard;
    ViewController *vc;
    mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"Home"];
    vc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    vc.isBookMark = YES;
    [[self navigationController]pushViewController:vc animated:YES];
    
}
- (IBAction)backPressed:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
    
}

-(BOOL)hideOpenBookMarkWhenTableDoesNotContianRecord{
 NSArray *array = [Utility fetchDatafromTable:@"BookMark" predicate:nil];
    if ([array count]==0) {
        [self.view viewWithTag:13].hidden=YES;
        return YES;
    }
    return NO;
}


@end
