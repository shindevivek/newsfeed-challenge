//
//  feedTableViewCell.h
//  DailyFeed
//
//  Created by Vivek LogTera on 31/10/15.
//  Copyright © 2015 Logtera. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol TableCellDelegate
@optional
- (void)showDetailsofTopic:(UIButton *)sender;
@end


@interface feedTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *title;
@property (strong, nonatomic) IBOutlet UIImageView *loadImageFromUrl;
@property (weak, nonatomic) IBOutlet UIButton *detailButton;
@property (nonatomic, strong) id  delegate;

@end
