# README #

### What is this repository for? ###

#  Develop a pseudo iOS application which would let users help list and read daily updates conveniently. #

[Minimum Requirement]

— Use of Web API to fetch feed details.

API Parameters:
*feed[] — {title, source, category, image, content, url}

*title AS string

*source AS string (feed source)

*category AS string (feed category).

*image AS url (URL containing image)

*content AS text (long string containing news content)

*url AS text (containing url)

— Visually interactive design to list feed items.

— An option to filter article category wise.

— Implement a Search feature to help users search articles via title or source.

— Submit Screenshots, Source code & detailed instructions.


### How do I get set up? ###

* Configuration
   Ideal xcode test environment
   Target Deployment: 7.0+ 
   Test in iPhone 4S Simulator.
   coding language:  **objective-c.**

* Other guidelines
Select the build target iPhone 4S

### Who do I talk to? ###

* Repo owner or admin  * Vivek Shinde *